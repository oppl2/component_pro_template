<!--
 * @Author: Yinjie Lee
 * @Date: 2022-04-17 20:46:15
 * @LastEditors: Yinjie Lee
 * @LastEditTime: 2022-04-23 22:03:00
-->
# cppware_pro_template

## 概述

作为一个模版，用于生产cpp工程项目所需要的通用的配置文件，脚本文件，目录层级结构等。使用此工具的目的：

1. 简化开发工作，自动生成工程所需要的文件和目录；
2. 标准化cpp工程通用脚本，配置文件，目录层级结构，使其符合统一的标准，提高代码质量和项目管理质量；
3. 为项目管理，项目运维实现自动化提供支持；

## 模版亮点

1. 完整的项目框架，具备unitest框架，Angular提交信息规范检查，CPPLINT检查，cmake框架等，易扩充
2. 快速开发验证，高效的编译和测试
3. 完善的conan使用，conan+cmake的精简示范,配合CI/CD发布conan包，快速完成版本交付
4. vscode 支持函数跳转

## 目录结构

使用此工具，生成的标准化目录层级结构如下：

``` shell

```

## 模版使用方法

工具基于conan实现，需要系统中安装opdocker环境或普通conan环境。使用方法如下：

``` shell
#opdocker 环境
opdocker
conan new myproject/0.0.0 --template=cppware
exit
sudo chown -R username:username myproject
sudo chmod u+x myproject/**/*.sh
./deploy_conan.sh
```

``` shell
#普通conan 环境
rm -rf ~/.conan/templates
conan config install https://gitlab.com/oppl2/cppware_pro_template.git
conan new myproject/0.0.0 --template=cppware
sudo chmod u+x myproject/**/*.sh
./deploy_conan.sh
```

## 编译

执行以下命令，编译产物在./build，编译安装目录在./install

```shell
cd myproject/
opdocker
./build.sh [-t <x64|aarch64>] [-b <release|debug>]
```

## 运行单元测试

``` shell
./build.sh -t x64 -b debug
./run_all_unitests.sh
```

模板目前支持多种平台，包括：aarch64，x64，如果需要验证代码是否适配上述所有平台的编译和打包流程，直接执行如下脚本：

``` shell
./scripts/build_check.sh
```


