#!/bin/bash
###
 # @Author: Yinjie Lee
 # @Date: 2022-04-17 20:46:16
 # @LastEditors: Yinjie Lee
 # @LastEditTime: 2022-04-18 02:53:41
###

set -e

this_script_path=$(cd `dirname $0`; pwd)

command -v gcovr >/dev/null 2>&1

function run_all_unitests() {
  for file in `ls $1`;
  do
    if [ -d "$file" ]; then
      echo "$2$file"
    else
      if [ -x "$file" ]; then
        echo $1/$file
        sh -c $1/$file
      fi

    fi
  done
}

export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${this_script_path}/build/x64_Debug/lib/:/root/x64/lib/
echo $LD_LIBRARY_PATH

echo "${this_script_path}/build/x64_Debug/test/unitest/"
cd ${this_script_path}/build/x64_Debug/test/unitest/

run_all_unitests ./

cd -

mkdir -p install/coverage_report
# coverage summary
gcovr --config gcovr.cfg  \
 --object-directory build/x64_Debug

# coverage html
gcovr --config gcovr.cfg \
    --html --html-details -o install/coverage_report/index.html \
    --object-directory build/x64_Debug

echo -e "\033[32m ========================================================== \033[0m"
echo -e "\033[32m Congratulations!!! All unit tests passed :) \033[0m"
echo -e "\033[32m ========================================================== \033[0m"
