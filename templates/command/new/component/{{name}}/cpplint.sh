#!/bin/bash
###
 # @Author: Yinjie Lee
 # @Date: 2022-04-17 20:46:15
 # @LastEditors: Yinjie Lee
 # @LastEditTime: 2022-04-17 22:25:01
###

command -v cpplint >/dev/null 2>&1
if [ "$?" -ne 0 ]; then
  echo 'cpplint is not installed, please use the following command:'
  echo -e '\n\n\tpip3 install cpplint\n\n'
  exit 1
fi

echo -e "\033[32m cpplint running... \033[0m"
# TODO(dash):  Wait until all codes are processed, change to 'cpplint --quiet --recursive *'

list=("include" "src" "example" "test")

for value in ${list[@]}
do
  echo $value
  cpplint --quiet --recursive $value
  if [ $? -ne 0 ]; then
    echo -e "\033[32m cpplint failed :( \033[0m"
    exit 1
  fi
done

echo -e "\033[32m ========================================================== \033[0m"
echo -e "\033[32m        Congratulations!!! cpplint all codes passed :) \033[0m"
echo -e "\033[32m ========================================================== \033[0m"
