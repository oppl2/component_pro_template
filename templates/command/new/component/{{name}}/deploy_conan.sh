#!/bin/bash
SCRIPTPATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
COMPONENT_TEMPLATE="CmpTemplateEmpty"
CONAN_COMPONENT="component/1.0.0@zksd/stable"

conan remove "${CONAN_COMPONENT}" -f
conan install ${CONAN_COMPONENT} -o template_path="${SCRIPTPATH}/${COMPONENT_TEMPLATE}" -o component_to="CmpSinUaServer" -if . --build missing

echo -e "\033[32m ========================================================== \033[0m"
echo -e "\033[32m Congratulations!!! All unit tests passed :) \033[0m"
echo -e "\033[32m ========================================================== \033[0m"

