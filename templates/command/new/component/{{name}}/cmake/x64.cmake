# cmake for platform x64

add_library({{name}}
  src/{{name}}.c
)

# add_definitions(-D LINUX)
# add_definitions(-DCDECL=\"AAAAA\")

# install config file
install(
  FILES config/x64.json
  DESTINATION etc/{{name}}
  RENAME {{name}}.json
)

# install binnary
install(
  TARGETS {{name}}
  DESTINATION lib
)
