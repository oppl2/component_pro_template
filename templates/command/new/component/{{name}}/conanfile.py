from conans import ConanFile, CMake, tools
from conans.errors import ConanInvalidConfiguration
import semver
required_conan_version = ">=1.33.0"


def get_version():
    git = tools.Git()
    try:
        # If the last commit is not a tag, git.get_tag() returns None
        if git.get_tag() != None:
            return git.get_tag()
        else:
            return ("%s_%s" % (git.get_branch(), git.get_revision()[0:7])).replace(' ','').replace('(','').replace(')','')
    except:
        return ""

class {{ name|capitalize }}Conan(ConanFile):
    name = "{{name}}"
    branch = "master"
    url = "https://gitlab.com/oppl2/{{name}}"
    generators = "cmake", "cmake_find_package", "cmake_paths"
    settings = "os", "arch", "compiler", "build_type"
    _cmake = None
    options = {
        "shared": [True, False],
        "fPIC": [True, False],
        "no_main": [True, False],
    }
    default_options = {
        "shared": True,
        "fPIC": True,
        "no_main": False,
    }

    @property
    def _cmake_install_base_path(self):
        return "cmake"
    def init(self):
        try:
            self._semver = semver.semver(get_version(),False)
        except:
            self._semver = semver.semver("0.0.0",False)
        self.version = self._semver.version
    def configure(self):
        if self.options.shared:
            # self.options["###"].shared = self.options.shared
            del self.options.fPIC

    def source(self):
        self.run("git clone https://GIT_ACCESS_TOKEN:%s@gitlab.com/oppl2/{{name}}.git && cd {{name}} && git checkout %s" %
                 (tools.get_env("GIT_ACCESS_TOKEN"), self.branch))

    def requirements(self):
        self.requires("codesys/1.0.0@sincegye/test")

    def _configure_cmake(self):
        if self._cmake:
            return self._cmake
        self._cmake = CMake(self, generator='Ninja')
        self._cmake.definitions["CMAKE_BUILD_TYPE"] = self.settings.build_type
        self._cmake.definitions["BUILD_UNITTEST"] = True if self.settings.build_type == "Debug" else False
        self._cmake.definitions["{{name|upper}}_VERSION_MAJOR"] = self._semver.major
        self._cmake.definitions["{{name|upper}}_VERSION_MINOR"] = self._semver.minor
        self._cmake.definitions["{{name|upper}}_VERSION_PATCH"] = self._semver.patch
        if self._semver.prerelease:
            self._cmake.definitions["{{name|upper}}_VERSION_PRERELEASE"] = '.'.join(str(e) for e in self._semver.prerelease)
        if self._semver.build:
            build_str=('.'.join(str(e) for e in self._semver.build))
            if build_str != self.channel:
                raise ConanInvalidConfiguration("Semver version(%s) is not equal to current branch(%s)" % (build_str,self.channel))
            self._cmake.definitions["{{name|upper}}_VERSION_BUILD"] = build_str
        self._cmake.configure(source_folder=self.source_folder)
        return self._cmake

    def build(self):
        cmake = self._configure_cmake()
        cmake.build()
        cmake.install()

    def package(self):
        self.copy("*", src="include", dst="include")
        self.copy("*", src="doc",dst="doc/{{name}}")
        self.copy("*.sdf", src="config",dst="sdf")

    def imports(self):
        self.copy("*.so*", src="", dst="")

    def deploy(self):
        self.copy("*", src="lib", dst="lib")
        self.copy("*", src="include", dst="include")
        self.copy("*",src="cmake", dst="cmake")

    def package_info(self):
        self.cpp_info.names["cmake_find_package"] = "{{name}}"
        if self.settings.build_type == "Debug":
            self.cpp_info.libs = ["config_factory","gmock_maind","gmockd","gtest_maind","gtestd","jsoncpp"]
        else:
            self.cpp_info.libs = ["config_factory","gmock_main","gmock","gtest_main","gtest","jsoncpp"]



